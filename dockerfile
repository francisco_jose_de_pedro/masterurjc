FROM ruby:2.7.0
RUN apt-get update && apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get update && apt-get install -y nodejs
RUN npm install --global yarn
RUN mkdir /myapp
WORKDIR /app
COPY . .
RUN rm -Rf node_modules/
RUN yarn install --check-files
RUN bundle install
EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]